"""Lazily-named module with tools for manipulating and inspecting dict structures

Many of the tools here will work with all (or most) mutable and non-mutable
mappings.

☭ Communist Content"""
from collections.abc import Mapping
from typing import Any, Iterator
import graphlib
from .util import binarytree as bt

__all__ = ["DictHeap",
           "dictheap_replace",
           "factory_replace",]


class DictHeap(Mapping):
    """DictHeaps are one-dimensional representaions of nested dict structures.

    DictHeap is a Mapping of integer indices to `(key, object)` item tuples
    such that:

    >>> dh = DictHeap(mapping)
    >>> dh[-1][1].__getitem__(dh[0][0]) is dh[0][1]
    ... True
    >>>
    >>> # The above is equivalent to:
    >>> parent = dh.get_data(dh.get_parent_node(0)) # dh[-1][1]
    >>> key = dh.get_real_key(0) # dh[0][0]
    >>> object = dh.get_data(0) # dh[0][1]
    >>> parent[key] is object
    ... True
    """
    # These are convenient constants for readability
    # It makes no sense ot change most of them, and I didn't extract them
    # so that they could be changed; but a few might have interesting
    # sde-effects.
    _TOP_NODE = -1
    """The node ID for the top-level dict"""
    _BT_ROOT_NODE = 0
    """ID to use as the root -- math should be the same"""
    _TOP_NODE_PARENT = None
    """Always None"""
    _TOP_NODE_REALKEY = None
    """Always None"""
    _TOP_NODE_SIBLINGS = list()
    """Always an empty list"""
    _TOP_NODE_LEVEL = 0
    """Level index -- set to -1 for ID 0 to be at Level 0."""
    @property
    def graph(self):
        """`graphlib`-style graph of the dict structure"""
        graph = {}
        nodes = list(self)
        while len(nodes) > 0:
            node = nodes.pop(0)
            graph[node] = self.get_child_nodes(node)
        return graph

    def __init__(self, mapping: Mapping, *, include_data=False):
        """DictHeap instances are representations of a dict structure. The
        original structure still exists. DictHeap instances will not reflect
        changes to the structure (hint: you can call their __init__ a second
        time).

        DictHeap instances have a references to each object in the original
        mapping structure, so if you want to delete the structure, delete
        affiliated DictHeaps too.

        If include_data is True, all objects (not just mappings) will be
        included. By default, dictheap only includes mappings.
        """
        self._contents = self.heapify_mapping(
            mapping, include_data=include_data)
        self.topological_order = list(
            graphlib.TopologicalSorter(self.graph).static_order())
        """Topologically-sorted list of nodes"""

    def __repr__(self):
        return repr(self._contents)

    def __str__(self):
        guts = ", ".join(
            f"{k}: {type(self.get_data(k)).__name__}" for k in self)
        return f"DictHeap({guts})"

    def __getitem__(self, key: Any) -> tuple[int | None, Mapping]:
        return self._contents[key]

    def __iter__(self) -> Iterator:
        return iter(sorted(self._contents.keys()))

    def __len__(self) -> int:
        return len(self._contents)

    @staticmethod
    def heapify_mapping(mapping: Mapping,
                        *,
                        include_data: bool = False,
                        _num: int = None,
                        _include_top_level: bool = True) -> dict[int, tuple[Any, Mapping] | tuple[Any, Any]]:
        """Create the DictHeap structure (but not a DictHeap object).

        DictHeaps internally use the output of heapify_mapping during
        initialization.

        If `include_data` is True, non-dict items will be included in the
        output. (False by default)

        Passing internal arguments `_num` and `_include_top_level` is not
        recommended; they are used for recursively finding sub-mappings.
        """
        def recurse_for_mapping(num: int, heap: dict, _object, include_data: bool):
            if isinstance(_object, Mapping):
                heap.update(DictHeap.heapify_mapping(
                    _object,
                    _num=bt.right(
                        num) if num > DictHeap._TOP_NODE else DictHeap._BT_ROOT_NODE,
                    _include_top_level=False,
                    include_data=include_data,))

        _num = _num if _num is not None else DictHeap._TOP_NODE
        heap = dict()
        data = {DictHeap._TOP_NODE_REALKEY: mapping} if _include_top_level else mapping

        for k, v in data.items():
            if include_data or isinstance(v, Mapping):
                heap[_num] = (k, v)
                recurse_for_mapping(_num, heap, v, include_data)
                _num = bt.left(_num)

        return heap

    def get_level(self, node: int) -> int:
        """Get the data's depth within the nested structure

        -1's depth is 0, and the nested portion of the structure starts at 1.
        For what it's worth, that mean's that each node `i`, the depth is the
        Hamming weight of `i+1` -- there might be a more clever way to find it
        than by iterating over a binary tree."""
        if node not in self:
            raise KeyError
        if node == DictHeap._TOP_NODE:
            return DictHeap._TOP_NODE_LEVEL
        return sum(map(bt.parity, bt.walk_from(node)[:-1]))+1+DictHeap._TOP_NODE_LEVEL

    def get_sibling_nodes(self, node: int) -> list:
        """List the DictHeap nodes of this node's data's sibling data

        -1 has no siblings by definition."""
        if node is DictHeap._TOP_NODE:
            return DictHeap._TOP_NODE_SIBLINGS
        siblings = self.get_child_nodes(self.get_parent_node(node))
        siblings.remove(node)
        return siblings

    def get_parent_node(self, node: int) -> int:
        """Get the DictHeap node of this node's data's parent dict

        -1's parent is None"""
        if node == DictHeap._TOP_NODE:
            return DictHeap._TOP_NODE_PARENT
        working_node = node
        while bt.parity(working_node) == bt.Parity.LEFT:
            working_node = bt.parent(working_node)
        else:
            working_node = bt.parent(working_node)

        return working_node if working_node is not bt.INVALID else DictHeap._TOP_NODE

    def get_child_nodes(self, node: int) -> list:
        """List the DictHeap nodes of this dict's children dicts"""
        children = []
        if node == DictHeap._TOP_NODE:
            working_node = DictHeap._BT_ROOT_NODE
        else:
            working_node = bt.right(node)
        while working_node in self:
            children.append(working_node)
            working_node = bt.left(working_node)
        return children

    def get_real_key(self, node: int) -> Any:
        """Return the parent dict's key which maps to this node's data

        -1's "real key" is None"""
        return self[node][0]

    def get_data(self, node: int) -> Mapping:
        """Get this dict (or other data)"""
        return self[node][1]

# Some functions that don't belong in DictHeap beacuse they manipulate the
# dict significantly


def dictheap_replace(dictheap: DictHeap, node: int, new_value, *, refresh: bool = False) -> None:
    """For a node in a given DictHeap, replace its object with a different object.

    `dictheap` must be a DictHeap object.
    
    If `refresh` is True, or if node is -1, calls `dictheap.__init__` again, to
    reflect the new structure, in case any mappings were replaced (default is False)."""
    if node == DictHeap._TOP_NODE:
        dictheap.__init__(new_value)
    else:
        parent_dict = dictheap.get_data(dictheap.get_parent_node(node))
        parent_dict[dictheap.get_real_key(node)] = new_value
        if refresh:
            dictheap.__init__(dictheap.get_data(DictHeap._TOP_NODE))


def factory_replace(mapping: Mapping, func) -> Any:
    """Runs `func()` on every Mapping found in `mapping`'s values, in-place.

    There should be no cycles in a graph of the structure.

    factory_replace does the following:

    1. Creates a DictHeap of mapping, not including data
    2. Loops over every Mapping in the DictHeap, in topological order (so that
    the deepest objects are replaced first)
        1. calls dictheap_replace, replacing the object with
        `func(object)`
    
    This is probably most useful for casting nested Mappings into a new type
    of Mapping.
    """
    dictheap = DictHeap(mapping)
    for node in dictheap.topological_order[:-1]:
        _dict = dictheap.get_data(node)
        dictheap_replace(dictheap, node, func(_dict))

    return func(dictheap.get_data(DictHeap._TOP_NODE))
