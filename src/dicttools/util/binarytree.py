"""Some functions related to binary trees.

For all functions, input < 0 returns `binarytree.INVALID`.
Some may return INVALID in other circumstances, see docs.

☭ Communist Content
"""
from collections import defaultdict
from math import floor, log
from enum import IntEnum, global_enum

INVALID = object()
"""returned for node < 0, or in some cases node <= 0"""
_SWITCH_LR = False


class Parity(IntEnum):
    """Left and Right values for parity()"""
    LEFT = 0 if not _SWITCH_LR else 1
    RIGHT = 1 if not _SWITCH_LR else 0


def depth(node: int) -> int | object:
    """node's depth in binary tree, with 0-indexed lines"""
    if node < 0:
        return INVALID
    return int(floor(log(node+1, 2)))


def line_index(node: int) -> int | object:
    """node's position in its own line"""
    if node < 0:
        return INVALID
    return int((node+1) % (2**depth(node)))


def parity(node: int) -> int | object:
    """node parity, 0 (left) or 1 (right)

    0's parity is binarytree.INVALID"""
    if node <= 0:
        return INVALID
    return int((node+1) % 2)


def parent(node: int) -> int | object:
    """node's parent node

    0's parent is binarytree.INVALID"""
    if node <= 0:
        return INVALID
    return int((node+node % 2)/2-1)


def left(node: int) -> int | object:
    """node's left child"""
    if node < 0:
        return INVALID
    return int(node*2+1+Parity.LEFT)


def right(node: int) -> int | object:
    """node's right child"""
    if node < 0:
        return INVALID
    return int(node*2+1+Parity.RIGHT)


def walk_from(node: int) -> list[int] | object:
    """list all nodes encountered walking from k to 0"""
    if node < 0:
        return INVALID
    out = [node]
    while parent(node) is not INVALID:
        node = parent(node)
        out.append(node)
    return out


def graph(nodes: list[int]) -> dict[int, list[int]]:
    """graphlib graph of list, assuming list of binary tree indices"""
    out = defaultdict(list)
    for node in nodes:
        if left(node) in nodes:
            out[node].append(left(node))
        if right(node) in nodes:
            out[node].append(right(node))

    return dict(out)


def inorder_nodes(nodes: list) -> list[int]:
    """ordered list of nodes as bt indices, depth-first in-order"""
    node = 0
    stack = []
    out = []
    while len(stack) > 0 or node in nodes:
        if node in nodes:
            stack.append(node)
            node = left(node)
        else:
            node = stack.pop(-1)
            out.append(node)
            node = right(node)

    return out
