#!/usr/bin/env python3
"""Use turtle to draw a DictHeap

This generated dictheap_visualization.png"""
import turtle as t
from turtle import mainloop
from dicttools.util import binarytree as bt

def level(node):
    return sum(map(bt.parity, bt.walk_from(node)[:-1]))+1

def draw_bars(screensize, bar_height, xscale, yscale):
    bar_pen = t
    bar_pen.shape("point")
    bar_pen.penup()
    bar_pen.speed(0)
    bar_pen.goto(0, screensize[1]+bar_height)
    node = 0

    for _ in range(int(screensize[1] / bar_height)):
        parts = int(node + 1)
        bar_pen.shapesize(bar_height*yscale, screensize[0]*xscale/parts)
        bar_pen.goto(0-screensize[0]/parts, bar_pen.pos()[1]-bar_height)
        bar_pen.setheading(0)
        _parts = parts
        while parts > 0:
            color = int(min(max(  255/ level(node)  , 0), 255))
            parts -= 1
            node += 1
            bar_pen.forward(screensize[0]/_parts)
            bar_pen.color((max(color-20,0), max(color-10,0), color))
            bar_pen.stamp()

def draw_tree(screensize, bar_height, levels):
    def write_label(this_node, this_level):
        if this_level <= 2**5:
            tree_pen.color("black")
        else:
            tree_pen.color("white")
        if this_level <= 2**5:
            tree_pen.write(this_node,font=("Arial", 12, "bold"))
        elif this_level <= 2**6:
            tree_pen.write(this_node,font=("Arial", 9, "normal"))
        else:
            tree_pen.write(this_node,font=("Arial", 5, "normal"))
        tree_pen.color("dark red")
    tree_pen = t
    tree_pen.shapesize(tree_pen.getscreen().xscale, tree_pen.getscreen().yscale)    
    tree_pen.shape("classic")
    tree_pen.color("dark red")
    tree_pen.pensize(2)
    tree_pen.showturtle()
    tree_pen.penup()

    queue = [(screensize[0]/2, screensize[1]-bar_height*0.5, 1, 1, 0)]
    while len(queue) > 0:
        this_x, this_y, this_level, this_cell, this_node = queue.pop(0)
        if this_level > 2**(levels-2):
            tree_pen.hideturtle()
            break
        tree_pen.goto((this_x, this_y))
        this_pos = tree_pen.pos()

        next_level = this_level*2
        next_cell_l = this_cell*2-1
        next_cell_r = this_cell*2
        next_cellsize = screensize[0]/next_level
        half_next_cellsize = next_cellsize/2
        next_lx = half_next_cellsize+(next_cellsize*(next_cell_l-1))
        next_rx = half_next_cellsize+(next_cellsize*(next_cell_r-1))
        next_y = this_y-bar_height
        next_left_loc = (next_lx, next_y, next_level, next_cell_l, this_node*2+1)
        next_right_loc = (next_rx, next_y, next_level, next_cell_r, this_node*2+2)

        queue.extend([next_left_loc, next_right_loc])
        tree_pen.pendown()
        write_label(this_node, this_level)
        tree_pen.color()
        tree_pen.goto(queue[-2][:2])
        write_label(queue[-2][4], queue[-2][2])
        tree_pen.penup()
        tree_pen.goto(this_pos)
        tree_pen.pendown()
        tree_pen.goto(queue[-1][:2])
        write_label(queue[-1][4], queue[-1][2])
        tree_pen.penup()

def main():
    levels = 8
    screensize = (1800, 910)
    bar_height = screensize[1]/levels
    t.setup(screensize[0], screensize[1])
    t.bgcolor("dark blue")
    t.setworldcoordinates(10, 10, *screensize)
    t.register_shape("point", ((0, 0), (0, 1), (1, 1), (1, 0)))
    t.colormode(255)
    draw_bars(screensize, bar_height, t.getscreen().xscale, t.getscreen().yscale)
    draw_tree(screensize, bar_height*t.getscreen().yscale, levels)
    # t.getscreen().getcanvas().postscript(file="output.eps")

if __name__ == "__main__":
    message = main()
    mainloop()

