from collections import UserDict
import unittest
import dicttools


class DictHeap(unittest.TestCase):
    def setUp(self) -> None:
        """
        data = {8: {}, 
                9: {8: {},
                    9: {},
                    10: {8: {}},
                    11: {8: {}}}}
        
        dictheap = {-1: (None, self.data),
                     0: (8, self.dict_h),
                     1: (9, self.dict_g),
                     4: (8, self.dict_e),
                     9: (9, self.dict_f),
                     19: (10, self.dict_c),
                     39: (11, self.dict_d),
                     40: (8, self.dict_a),
                     80: (8, self.dict_b)}
        
        graph = {-1: [0, 1],
                 0: [],
                 1: [4, 9, 19, 39],
                 4: [],
                 9: [],
                 19: [40],
                 39: [80],
                 40: [],
                 80: []}
        
        topo_order = [0, 4, 9, 40, 80, 19, 39, 1, -1]
        
        """
        self.dict_a = {}
        self.dict_b = {}
        self.dict_c = {8: self.dict_a}
        self.dict_d = {8: self.dict_b, 3: "five"}
        self.dict_e = {}
        self.dict_f = {}
        self.dict_g = {1: "one", 2: "two", 8: self.dict_e, 9: self.dict_f, 10: self.dict_c, 11: self.dict_d}
        self.dict_h = {4: "four"}
        self.data = {8: self.dict_h, 9: self.dict_g, 22: "twenty-two"}
        self.dictheap = dicttools.DictHeap(self.data)
    
    def test_get_level(self):
        test = self.dictheap.get_level(40)
        expected = 3
        self.assertEqual(test, expected)
        test = self.dictheap.get_level(19)
        expected = 2
        self.assertEqual(test, expected)
        test = self.dictheap.get_level(1)
        expected = 1
        self.assertEqual(test, expected)
        test = self.dictheap.get_level(-1)
        expected = 0
        self.assertEqual(test, expected)

    def test_str(self):
        test = str(self.dictheap)
        expected = "DictHeap(-1: dict, 0: dict, 1: dict, 4: dict, 9: dict, 19: dict, 39: dict, 40: dict, 80: dict)"
        self.assertEqual(test, expected)
    
    def test_repr(self):
        test = repr(self.dictheap)
        expected = "\
{-1: (None, {8: {4: 'four'}, 9: {1: 'one', 2: 'two', 8: {}, 9: {}, 10: {8: {}}, 11: {8: {}, 3: 'five'}}, 22: 'twenty-two'}), \
0: (8, {4: 'four'}), 1: (9, {1: 'one', 2: 'two', 8: {}, 9: {}, 10: {8: {}}, 11: {8: {}, 3: 'five'}}), \
4: (8, {}), 9: (9, {}), 19: (10, {8: {}}), 40: (8, {}), 39: (11, {8: {}, 3: 'five'}), 80: (8, {})}"
        self.assertEqual(test, expected)

    def test_heapify_mapping_incl_data(self):
        test = dicttools.DictHeap.heapify_mapping(self.data, include_data=True)
        expected = {-1: (None, self.data),
                    0: (8, self.dict_h),
                    1: (9, self.dict_g),
                    3: (22, "twenty-two"),
                    2: (4, "four"),
                    4: (1, "one"),
                    9: (2, "two"),
                    19: (8, self.dict_e),
                    39: (9, self.dict_f),
                    79: (10, self.dict_c),
                    159: (11, self.dict_d),
                    160: (8, self.dict_a),
                    320: (8, self.dict_b),
                    641: (3, "five")}
        
        self.assertEqual(test, expected)
        self.assertIs(test[160][1], self.dict_a)

    def test_heapify_mapping(self):
        test = dicttools.DictHeap.heapify_mapping(self.data)
        expected = {-1: (None, self.data),
                    0: (8, self.dict_h),
                    1: (9, self.dict_g),
                    4: (8, self.dict_e),
                    9: (9, self.dict_f),
                    19: (10, self.dict_c),
                    39: (11, self.dict_d),
                    40: (8, self.dict_a),
                    80: (8, self.dict_b)}
        
        self.assertEqual(test, expected)
        self.assertIs(test[40][1], self.dict_a)

    def test_get_real_key(self):
        test = self.dictheap.get_real_key(19)
        expected = 10
        self.assertEqual(test, expected)
        test = self.dictheap.get_real_key(-1)
        expected = None
        self.assertEqual(test, expected)
    
    def test_get_dict(self):
        test = self.dictheap.get_data(19)
        expected = self.dict_c
        self.assertIs(test, expected)
        test = self.dictheap.get_data(-1)
        expected=self.data
        self.assertIs(test, expected)

    def test_get_sibling_nodes(self):
        test = self.dictheap.get_sibling_nodes(19)
        test.sort()
        expected = [4, 9, 39]
        self.assertEqual(test, expected)
        test = self.dictheap.get_sibling_nodes(-1)
        expected = []
        self.assertEqual(test, expected)
        test = self.dictheap.get_sibling_nodes(0)
        test.sort()
        expected = [1]
        self.assertEqual(test, expected)
        test = self.dictheap.get_sibling_nodes(1)
        test.sort()
        expected = [0]
        self.assertEqual(test, expected)

    def test_get_parent_node(self):
        test = self.dictheap.get_parent_node(19)
        expected = 1
        self.assertEqual(test, expected)
        test = self.dictheap.get_parent_node(39)
        expected = 1
        self.assertEqual(test, expected)
        test = self.dictheap.get_parent_node(40)
        expected = 19
        self.assertEqual(test, expected)
        test = self.dictheap.get_parent_node(1)
        expected = -1
        self.assertEqual(test, expected)
        test = self.dictheap.get_parent_node(0)
        expected = -1
        self.assertEqual(test, expected)
        test = self.dictheap.get_parent_node(-1)
        expected = None
        self.assertEqual(test, expected)
    
    def test_get_child_nodes(self):
        test = self.dictheap.get_child_nodes(1)
        expected = [4, 9, 19, 39]
        self.assertEqual(test, expected)
        test = self.dictheap.get_child_nodes(-1)
        expected = [0, 1]
        self.assertEqual(test, expected)

    def test_as_graph(self):
        test = self.dictheap.graph
        expected = {-1: [0, 1], 0: [], 1: [4, 9, 19, 39], 4: [], 9: [], 19: [40], 39: [80], 40: [], 80: []}
        self.assertEqual(test, expected)
        test = self.dictheap.topological_order
        expected = [0, 4, 9, 40, 80, 19, 39, 1, -1]
        self.assertEqual(test, expected)

class other_dicttools(unittest.TestCase):
    def setUp(self) -> None:
        self.dict_a = {}
        self.dict_b = {}
        self.dict_c = {8: self.dict_a}
        self.dict_d = {8: self.dict_b}
        self.dict_e = {}
        self.dict_f = {}
        self.dict_g = {8: self.dict_e, 9: self.dict_f, 10: self.dict_c, 11: self.dict_d}
        self.dict_h = {}
        self.data = {8: self.dict_h, 9: self.dict_g}
        self.dictheap = dicttools.DictHeap(self.data)

    def test_replace_dict(self):
        dicttools.dictheap_replace(self.dictheap, 1, None, refresh=True)
        test = self.data[9]
        expected = None

        self.assertIs(test, expected)
        self.assertIsInstance(self.dict_h, dict)
        self.assertEqual(len(self.dictheap.get_child_nodes(-1)), 1)

    def test_factory_replace(self):
        class testclass(UserDict): ...
        test = dicttools.factory_replace(self.data, testclass)
        self.dictheap = dicttools.DictHeap(test)
        self.assertIsInstance(self.data, dict)
        self.assertIsInstance(test, testclass)
        self.assertEqual(test, self.data)
        self.assertIs(test, self.dictheap.get_data(-1))
        self.assertIsInstance(self.data[8], testclass)
        self.assertIs(test[8], self.data[8])
        self.assertIsInstance(self.data[9], testclass)
        self.assertIs(test[9], self.data[9])
        self.assertIsInstance(self.data[9][8], testclass)
        self.assertIs(test[9][8], self.data[9][8])
        self.assertIsInstance(self.data[9][9], testclass)
        self.assertIs(test[9][9], self.data[9][9])
        self.assertIsInstance(self.data[9][10], testclass)
        self.assertIs(test[9][10], self.data[9][10])
        self.assertIsInstance(self.data[9][10][8], testclass)
        self.assertIs(test[9][10][8], self.data[9][10][8])
        self.assertIsInstance(self.data[9][11], testclass)
        self.assertIs(test[9][11], self.data[9][11])
        self.assertIsInstance(self.data[9][11][8], testclass)
        self.assertIs(test[9][11][8], self.data[9][11][8])
    
