# DictHeap Class #
The DictHeap class is a binary tree representing a nested dict structure with no cycles.

One use for a DictHeap is to avoid recursing over nested dict structures repeatedly. Instead, just make a DictHeap of the structure, and iterate over that. You can directly access any object in the nested structure without too much trouble or having to remember write around a long list of keys.

DictHeaps aren't literally heaps. It was naively named after heapq, since that's where I got the idea, and because it's too good as a name to change it

A nested dict structure with no cyclical references has (at least) these properties:

* Except for the top-level dict, every dict has a parent.
* Dicts may or may not have children, ie dicts in their values.
* All of a dict's children are unique in the structure.

We can represent this structure as a binary tree, by considering a third relationship in addition to child and parent: sibling. In our binary tree, each node represents a dict in the nested structure. Nodes have integer keys, for our purposes. For every node k:

* 2k+1 represents a sibling dict, contained in the same parent dict's values.
* 2k+2 represents a child dict, not to be confused with child nodes in the theoretical binary tree.

Consider the the root dict to be node k=-1. It definitionally has no siblings, so we don't run into issues with 2k+1 == -1! 2k+2 here is 0.

Node k=0 and *all* child nodes 2k+1 (1, 3, 7, 15, ...) are all of the children dicts of the root dict. Then, if the dict at node 0 has child dicts, they are represented by 2k+2 and all nodes 2k+1 from there (2, 5, 11, 23, ...)

## Example ##
```python
>>> data = {"level 0; object 0": int(0),
...         "level 0; object 1": {"level 1; object 1-0": int(2), "L 1; O 1-1": int(3), "L 1; O 1-2": int(4)},
...         "level 0; object 2": int(5),
...         "level 0; object 3": {"level 1; object 3-0": { "L 2; O 3-0-0": int(8), "L 2; O 3-0-1": int(9)},
...                               "L 1; O 3-1": int(10),
...                               "L 1; O 3-2":{ "L 2; O 3-2-0": int(12), "L 2; O 3-2-1": int(13)}}}
>>> DictHeap(data)
... # below is pseudo repr
...                                                                                            #  i (node)  expr  parent  sibling(s)
... DictHeap({-1:(None, {"level 0; object 0": 0, ...})                                         # -1               None    None
...           0:("level 0; object 1", {"level 1; object 1-0": 2, ...}),                        #  0        2k+2    -1      1
...           1:("level 0; object 3", {"level 1; object 3-0": {"L 2; O 3-0-0": 8, ...}, ...}), #  1        2k+1    -1      0
...           4:("level 1; object 3-0", {"L 2; O 3-0-0": 8, ...})                              #  4        2k+2     1      9
...           9:("L 1; O 3-2", {"L 2; O 3-2-0": 12, ...})})                                    #  9        2k+1     1      4
```

## Why? ##
The primary reason is that it's a pretty simple interface for what became a common task for me. It might also be fast -- I haven't run any real benchmarking on dictheap, but because we're mapping the dicts to binary tree node indices, a lot of operations become pretty easy with simple math, and a little bit of iteration. For example, finding out how deep a dict is within a nested dict structure:

![A function for finding the depth, expressed in mathematical terms, with a note indicating that a dict's level obtained by node index i is also the Hamming weight of i+1](dict_level.png)

The `dicttools.util.binarytree` module contains several functions operating on binary tree node indices in more-or-less pure math terms (as opposed to in object-oriented terms)