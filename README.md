# dicttools #
## Installation ##
dicttools is in version 1.0.0, I called it Good Enough. But I haven't uploaded it on pypi, 'cause I don't want to bother renaming it or contacting the author of the dicttools from a decade ago. Also because I haven't learned to do it yet.

So until I get over that/until there's a real reason for me to publish it (eg if I publish Menu, the project I needed dictheap for), you can install it with the following command:

`pip install git+https://gitlab.com/sb24kelley/dicttools.git#egg=dicttools`

Usage:
```python
import dicttools
```

The dicttools package contains:
* dicttools.DictHeap class
* * DictHeaps are one-dimensional representaions of nested dict structures; see doctstrings and doc/dictheap.md
* dicttools.dictheap_replace() function
* * For a node in a given DictHeap, replace its object with a different object.
* dicttools.factory_replace() function
* * Runs `func()` on every Mapping found in `mapping`'s values, in-place.
* dicttools.util.binarytree module
* * Functions mathematically describing an infinite binary tree